#include <stdio.h>
#include <stdlib.h>

int main()
{
   int num;
   printf("Input number: ");
   scanf("%d",&num);
   if (num>0)
   {
       printf("%d is a positive number",num);
   }
   if (num<0)
   {
       printf("%d is a Negative number",num);
   }
   if(num==0)
   {
       printf("Number is zero",num);
   }
   return 0;
}
